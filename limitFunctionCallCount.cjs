function limitFunctionCallCount(cb, n) {
  if (
    cb !== undefined &&
    typeof cb === "function" &&
    n !== undefined &&
    (typeof n === "number" || typeof n === "string") &&
    !isNaN(n)
  ) {
    let int_n = Math.floor(Number(n));

    return function invoke_cb(...args) {
      if (int_n > 0) {
        int_n--;
        return cb(...args);
      } else {
        return null;
      }
    };
  } else {
    throw new Error("Type error:");
  }
}

module.exports = limitFunctionCallCount;

function cacheFunction(cb) {
  if (cb !== undefined && typeof cb === "function") {
    let cache = {};
    return function invoke_cb(...n) {
      if (!(n in cache)) {
        let cb_result = cb(...n);

        cache[n] = cb_result;
        console.log(cache);
        return cb_result;
      } else {
        return cache[n];
      }
    };
  } else {
    throw new Error("Type Error:");
  }
}

module.exports = cacheFunction;

const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function cb(...args) {
  return null;
}

const invoke_cb_result = limitFunctionCallCount(cb, 2);
console.log(invoke_cb_result());
console.log(invoke_cb_result());
console.log(invoke_cb_result());

// try {
//   const invoke_cb_result = limitFunctionCallCount(cb, 5);
//   console.log(invoke_cb_result());
//   const result = invoke_cb_result();
//   console.log(result);
// } catch (error) {
//   throw new Error("Type error:");
// }

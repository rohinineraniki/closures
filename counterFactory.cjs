function counterFactory() {
  let initialValue = 0;
  function incrementOrDecrement(value) {
    initialValue += value;
    return initialValue;
  }
  return {
    increment: function increment() {
      return incrementOrDecrement(1);
    },

    decrement: function decrement() {
      return incrementOrDecrement(-1);
    },
  };
}
const counter_result = counterFactory();
counter_result.increment();
counter_result.increment();
let result = counter_result.increment();
console.log(result);

module.exports = counterFactory;
